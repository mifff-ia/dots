"""" C++ bindings """"
inoremap <leader>c ()<++><esc>F)i
inoremap <leader>s ""<++><esc>F"i
inoremap <leader>b {<cr>}<esc>O
" Include statements
inoremap <leader>ib #include <><cr><++><esc>kf<a
inoremap <leader>is #include ""<cr><++><esc>kf"a
inoremap <leader>d #define 
