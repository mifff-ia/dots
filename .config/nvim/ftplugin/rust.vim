"""" C++ bindings """"
inoremap <leader>c ()<++><esc>F)i
inoremap <leader>s ""<++><esc>F"i
inoremap <leader>b {<cr>}<esc>O
" Basic linter
call neomake#configure#automake('nw', 750)
