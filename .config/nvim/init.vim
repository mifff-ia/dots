""{{{ Configs
set autoindent
set conceallevel=2
set foldmethod=marker
set incsearch
set laststatus=2
set nohlsearch
set noshowcmd
set noshowmode
set scrolloff=3
set tildeop

" Numbering
set relativenumber
set number

" Tabbing
set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2
""}}}

""{{{ Plugins
call plug#begin('~/.local/share/nvim/plugged')
Plug 'vimwiki/vimwiki'
Plug 'itchyny/lightline.vim'
Plug 'dylanaraps/wal.vim'
Plug 'junegunn/goyo.vim'
Plug 'kovisoft/slimv'
"Plug 'scrooloose/nerdtree'
"Plug 'ehamberg/vim-cute-python'
"Plug 'w0rp/ale'
"Plug 'lervag/vimtex'
"Plug 'maximbaz/lightline-ale'
"Plug 'vim-airline/vim-airline'
call plug#end()
""}}}

""{{{ Plugin config
"" Colorscheme management
colorscheme wal

"" Lightline
let g:lightline = { 'colorscheme':'wal'}

"" Slimv
let g:slimv_swank_cmd = "!ros -e '(ql:quickload :swank) (swank:create-server)' wait &"
let g:slimv_lisp = 'ros run'
let g:slimv_impl = 'sbcl'
let g:paredit_electric_return=0
""}}}

""{{{ My bindings
let mapleader=';'
noremap <leader><leader> <esc>:NERDTreeToggle<cr>
"" Normal
nnoremap <c-e> 3<c-e>
nnoremap <c-y> 3<c-y>
nnoremap <leader>g :Goyo<cr>

"" Insert
"" Visual
""}}}

