import yaml
with (config.configdir / 'colors.yml').open() as f:
    yaml_data = yaml.load(f)
def dict_attrs(obj, path=''):
    if isinstance(obj, dict):
        for k, v in obj.items():
            yield from dict_attrs(v, '{}.{}'.format(path, k) if path else k)
    else:
        yield path, obj
for k, v in dict_attrs(yaml_data):
    config.set(k, v)

config.set('tabs.show', 'multiple')
config.set('tabs.favicons.show', 'never')
config.set('tabs.padding', {"top": 0, "bottom": 0, "left": 15, "right": 15})
config.set('tabs.indicator.width', 0)
config.set('tabs.title.format', '{title}')
