# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH
cat $HOME/.cache/wal/sequences

export ZSH=/home/max/.oh-my-zsh

export TERMINAL=alacritty

export EDITOR=nvim

ZSH_THEME="gianu"

plugins=(
  git
)

source $ZSH/oh-my-zsh.sh

# aliases
alias ninit='nvim ~/.config/nvim/init.vim'
alias n='nvim'
function ccomp(){
    gcc -Wall -O0 -g "$1.c" -o "$@"
}
alias mm='mv ~/Downloads/*.mp3 ~/music && mpc update &> /dev/null'
alias xi='sudo xbps-install'
alias xr='sudo xbps-remove'
alias xq='xbps-query'
alias sx="startx -- :1 vt$(tty | sed -e "s:/dev/tty::")"
